from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


def students_list(request):
    return render(request, 'students/students_list.html', {})


def students_add(request):
    ''' render Add form '''
    return HttpResponse('<h1>Student Add Form</h1>')


def students_edit(request, sid):
    ''' edit student '''
    return render(request, 'students/student_edit.html', {})


def students_delete(request, sid):
    ''' delete student '''
    return HttpResponse('<h1>Delete Student %s</h1>' % sid)


def groups_list(request):
    ''' show groups list '''
    return render(request, 'students/group_list.html', {})


def groups_add(request):
    ''' add group '''
    return HttpResponse('<h1>Group add</h1>')


def journals_list(request):
    ''' show student visits journal '''
    return render(request, 'students/journals.html', {})